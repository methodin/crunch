package com.coetre.crunch;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.activeandroid.query.Select;
import com.coetre.crunch.adapters.BudgetPagerAdapter;
import com.coetre.crunch.adapters.DrawerAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.fragments.ArchiveFragment;
import com.coetre.crunch.fragments.ListingFragment;
import com.coetre.crunch.fragments.OverviewFragment;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.DrawerActivity;
import com.coetre.crunch.utils.EntityChangeManager;

import java.lang.ref.WeakReference;
import java.util.List;

public class MainActivity extends DrawerActivity implements EntityChangeManager.EntityChangeListener {
    private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setIcon(R.drawable.ic_actionbar);
        }

        prefs = getBaseContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);

        DrawerItemClickListener drawerItemClickListener = new DrawerItemClickListener();
        drawerList.setOnItemClickListener(drawerItemClickListener);
        drawerItemClickListener.onItemClick(null, null, Config.mode, 0);
	}

    @Override
    protected void onResume() {
        super.onResume();
        EntityChangeManager.add(Budget.class, this);
        updateDrawerItems();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EntityChangeManager.remove(Budget.class, this);
    }

    /**
     * Update the drawer items
     */
    private void updateDrawerItems() {
        new DrawerLoader(drawerList).execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        // EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        // EasyTracker.getInstance().activityStop(this);
    }

    @Override
    public void change(Class<?> cls) {
        updateDrawerItems();
    }

    /**
     * Loads the drawer items
     */
    public class DrawerLoader extends AsyncTask<Integer, Void, Integer[]> {
        private final WeakReference<ListView> referenceDrawerList;

        public DrawerLoader(ListView drawerList) {
            referenceDrawerList = new WeakReference<ListView>(drawerList);
        }

        @Override
        protected Integer[] doInBackground(Integer... params) {
            Integer[] counts = new Integer[3];
            List<Budget> tasks = new Select().from(Budget.class).where("Deleted != 1").execute();
            counts[0] = tasks.size();
            counts[1] = tasks.size();
            tasks = new Select().from(Budget.class).where("Deleted = 1").execute();
            counts[2] = tasks.size();
            return counts;
        }

        /**
         * Set the ListView items in the UI thread
         */
        @Override
        protected void onPostExecute(Integer[] counts) {
            final ListView drawerList = referenceDrawerList.get();
            if(drawerList != null) {
                DrawerAdapter adapter = (DrawerAdapter) drawerList.getAdapter();
                for(int i=0;i<counts.length;i++) {
                    adapter.getItem(i).setCount(counts[i]);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * The drawer item click listener
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

        /** Swaps fragments in the overview content view */
        private void selectItem(int position) {
            Bundle bundle;
            switch(position) {
                case Config.MODE_DETAIL:
                    fragment = new OverviewFragment();
                    break;
                case Config.MODE_LISTING:
                    fragment = new ListingFragment();
                    break;
                case Config.MODE_ARCHIVE:
                    fragment = new ArchiveFragment();
                    break;
                case Config.MODE_SETTINGS:
                    if(!isDrawerLocked()) {
                        drawerLayout.closeDrawers();
                    }
                    drawerList.getOnItemClickListener().onItemClick(null, null, Config.mode, 0);
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    return;
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            // Highlight the selected item, update the title, and close the drawer
            drawerList.setItemChecked(position, true);
            setTitle(MainActivity.this.drawerItems[position]);
            if(!isDrawerLocked()) {
                drawerLayout.closeDrawer(drawerList);
            }

            Config.mode = position;
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(Config.PREFS_KEY_MODE, position);
            editor.commit();
        }
    }
}
