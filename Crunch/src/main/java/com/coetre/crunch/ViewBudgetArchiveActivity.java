package com.coetre.crunch;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.coetre.crunch.adapters.BudgetAdapter;
import com.coetre.crunch.adapters.BudgetArchiveAdapter;
import com.coetre.crunch.adapters.TransactionArchiveAdapter;
import com.coetre.crunch.entities.BudgetArchive;
import com.coetre.crunch.entities.TransactionArchive;
import com.coetre.crunch.utils.Formatter;

import java.util.List;

public class ViewBudgetArchiveActivity extends Activity {
    public static final String DATA_BUDGET_ARCHIVE_ID = "budgetArchiveId";
    private BudgetArchive budgetArchive;
    private List<TransactionArchive> transactions;
    private GridView listing;
    private View emptyView;
    private TextView totalView;
    private TextView currentView;
    private TextView percentageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        emptyView = findViewById(android.R.id.empty);
        totalView = (TextView) findViewById(R.id.total);
        currentView = (TextView) findViewById(R.id.current);
        percentageView = (TextView) findViewById(R.id.percentage);

        long id = getIntent().getLongExtra(DATA_BUDGET_ARCHIVE_ID, 0);
        if (id > 0) {
            budgetArchive = Model.load(BudgetArchive.class, id);
            transactions = new Select().from(TransactionArchive.class).where("BudgetArchive = ?", id).execute();

            totalView.setText("$"+Integer.toString(budgetArchive.getAmount()));
            currentView.setText("$"+budgetArchive.getCurrentAmount());

            Formatter.setPercentageView(this, budgetArchive, percentageView, R.string.percent_of_budget_archive);

            TransactionArchiveAdapter adapter = new TransactionArchiveAdapter(getBaseContext(), R.layout.view_card, transactions);
            listing = ((GridView) findViewById(R.id.transaction_list));
            listing.setAdapter(adapter);
            listing.setSelector(new ColorDrawable(Color.TRANSPARENT));
            listing.setEnabled(false);

            emptyView.setVisibility(transactions.size() > 0 ? View.GONE : View.VISIBLE);
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}