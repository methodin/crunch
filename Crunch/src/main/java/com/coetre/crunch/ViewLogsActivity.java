package com.coetre.crunch;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.activeandroid.query.Select;
import com.coetre.crunch.adapters.BudgetAdapter;
import com.coetre.crunch.adapters.BudgetArchiveAdapter;
import com.coetre.crunch.entities.BudgetArchive;

import java.util.List;

public class ViewLogsActivity extends Activity implements AdapterView.OnItemClickListener  {
    public static final String DATA_BUDGET_ID = "budgetId";
    private List<BudgetArchive> budgets;
    private GridView listing;
    private View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        emptyView = findViewById(android.R.id.empty);

        long budgetId = getIntent().getLongExtra(DATA_BUDGET_ID, 0);
        if (budgetId > 0) {
            budgets = new Select().from(BudgetArchive.class).where("Budget = ?", budgetId).execute();

            listing = ((GridView) findViewById(R.id.budget_list));
            BudgetArchiveAdapter adapter = new BudgetArchiveAdapter(getBaseContext(), budgets);
            listing.setAdapter(adapter);
            listing.setOnItemClickListener(this);

            emptyView.setVisibility(budgets.size() > 0 ? View.GONE : View.VISIBLE);
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, ViewBudgetArchiveActivity.class);
        intent.putExtra(ViewBudgetArchiveActivity.DATA_BUDGET_ARCHIVE_ID, ((BudgetArchiveAdapter) listing.getAdapter()).getItem(i).getId());
        startActivity(intent);
    }
}