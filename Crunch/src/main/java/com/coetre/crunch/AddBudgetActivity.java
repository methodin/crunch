package com.coetre.crunch;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.utils.Alert;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;

public class AddBudgetActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);

        findViewById(R.id.cancel_button).setOnClickListener(this);
        findViewById(R.id.add_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.add_button:
                String name = ((EditText) findViewById(R.id.budget_name)).getText().toString();
                String amount = ((EditText) findViewById(R.id.budget_amount)).getText().toString();

                int actualAmount = 0;
                try {
                    actualAmount = Integer.parseInt(amount);
                } catch (NumberFormatException e) {
                }

                // Form validation
                if (name.equals("")) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_budget_name));
                } else if(actualAmount <= 0) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_budget_amount));
                } else {
                    Budget budget = new Budget(name, actualAmount);
                    budget.save();
                    EntityChangeManager.notify(Budget.class );

                    SharedPreferences prefs = getBaseContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putLong(Config.PREFS_BUDGET_ID, budget.getId());
                    editor.putLong(Config.PREFS_OVERVIEW_BUDGET_INDEX, budget.getId());
                    editor.commit();

                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
    }
}