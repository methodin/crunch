package com.coetre.crunch;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.activeandroid.Model;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.utils.Alert;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;

public class EditBudgetActivity extends Activity implements View.OnClickListener {
    public static final String DATA_BUDGET_ID = "budgetId";
    private Budget budget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);

        Button addButton = (Button) findViewById(R.id.add_button);
        addButton.setText(R.string.button_save);
        addButton.setOnClickListener(this);

        findViewById(R.id.cancel_button).setOnClickListener(this);

        long id = getIntent().getLongExtra(DATA_BUDGET_ID, 0);
        if(id > 0) {
            budget = Model.load(Budget.class, id);

            EditText editText = (EditText) findViewById(R.id.budget_amount);
            editText.setText(Integer.toString(budget.getAmount()));

            editText = (EditText) findViewById(R.id.budget_name);
            editText.setText(budget.getName());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.add_button:
                String name = ((EditText) findViewById(R.id.budget_name)).getText().toString();
                String amount = ((EditText) findViewById(R.id.budget_amount)).getText().toString();

                int actualAmount = 0;
                try {
                    actualAmount = Integer.parseInt(amount);
                } catch (NumberFormatException e) {
                }

                // Form validation
                if (name.equals("")) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_budget_name));
                } else if(actualAmount <= 0) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_budget_amount));
                } else {
                    budget.setAmount(actualAmount);
                    budget.setName(name);
                    budget.save();
                    EntityChangeManager.notify(Budget.class );
                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
    }
}