package com.coetre.crunch.actions;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.activeandroid.Model;
import com.coetre.crunch.R;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.BudgetArchive;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.entities.TransactionArchive;
import com.coetre.crunch.utils.EntityChangeManager;

import java.util.Calendar;
import java.util.List;

public class BudgetActions {
    /**
     * Deletes a budget
     *
     * @param context
     * @param id
     * @param listener
     */
    public static void delete(final Context context, final long id, final OnCompleteAction listener) {
        showConfirmDeletion(context, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Budget budget = Model.load(Budget.class, id);
                List<Transaction> transactions = budget.getTransactions();
                BudgetArchive archive = new BudgetArchive(budget, Calendar.getInstance().getTime());
                archive.save();
                for (Transaction transaction : transactions) {
                    new TransactionArchive(transaction, archive).save();
                    Model.delete(Transaction.class, transaction.getId());
                }
                budget.setCurrentAmount(0);
                budget.setDeleted(true);
                budget.save();
                EntityChangeManager.notify(Budget.class);
                if (listener != null) {
                    listener.completed();
                }
            }
        });
    }

    /**
     * Delete a list of budgets
     *
     * @param context
     * @param budgets
     * @param listener
     */
    public static void delete(final Context context, final List<Budget> budgets, final OnCompleteAction listener) {
        showConfirmDeletion(context, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                for (Budget budget : budgets) {
                    List<Transaction> transactions = budget.getTransactions();
                    BudgetArchive archive = new BudgetArchive(budget, Calendar.getInstance().getTime());
                    archive.save();
                    for (Transaction transaction : transactions) {
                        new TransactionArchive(transaction, archive).save();
                        Model.delete(Transaction.class, transaction.getId());
                    }
                    budget.setCurrentAmount(0);
                    budget.setDeleted(true);
                    budget.save();
                }
                EntityChangeManager.notify(Budget.class);
                if (listener != null) {
                    listener.completed();
                }
            }
        });
    }

    /**
     * Show the confirm deletion dialog
     *
     * @param context
     * @param listener
     */
    private static void showConfirmDeletion(final Context context, final DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirm_delete_budget_title))
                .setMessage(context.getResources().getString(R.string.confirm_delete_budget))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, null).show();
    }

    /**
     * Resets a budget
     *
     * @param context
     * @param id
     * @param listener
     */
    public static void reset(final Context context, final long id, final OnCompleteAction listener) {
        showConfirmReset(context, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Budget budget = Model.load(Budget.class, id);
                List<Transaction> transactions = budget.getTransactions();
                BudgetArchive archive = new BudgetArchive(budget, Calendar.getInstance().getTime());
                archive.save();
                for (Transaction transaction : transactions) {
                    new TransactionArchive(transaction, archive).save();
                    Model.delete(Transaction.class, transaction.getId());
                }
                budget.setCurrentAmount(0);
                budget.save();
                EntityChangeManager.notify(Budget.class);
                if (listener != null) {
                    listener.completed();
                }
            }
        });
    }

    /**
     * Show the confirm reset dialog
     *
     * @param context
     * @param listener
     */
    private static void showConfirmReset(final Context context, final DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirm_reset_budget_title))
                .setMessage(context.getResources().getString(R.string.confirm_reset_budget))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, null).show();
    }

    /**
     * Unarchives a budget
     *
     * @param context
     * @param id
     * @param listener
     */
    public static void unarchive(final Context context, final long id, final OnCompleteAction listener) {
        showConfirmUnrachive(context, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Budget budget = Model.load(Budget.class, id);
                budget.setDeleted(false);
                budget.save();
                EntityChangeManager.notify(Budget.class);
                if (listener != null) {
                    listener.completed();
                }
            }
        });
    }

    /**
     * Show the confirm unrachive dialog
     *
     * @param context
     * @param listener
     */
    private static void showConfirmUnrachive(final Context context, final DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirm_unarchive_budget_title))
                .setMessage(context.getResources().getString(R.string.confirm_unarchive_budget))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, null).show();
    }

    /**
     * Reset interface
     */
    public interface OnCompleteAction {
        public void completed();
    }
}