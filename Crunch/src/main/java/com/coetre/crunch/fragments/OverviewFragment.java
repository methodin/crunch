package com.coetre.crunch.fragments;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.coetre.crunch.AddBudgetActivity;
import com.coetre.crunch.AddTransactionActivity;
import com.coetre.crunch.EditBudgetActivity;
import com.coetre.crunch.R;
import com.coetre.crunch.ViewLogsActivity;
import com.coetre.crunch.actions.BudgetActions;
import com.coetre.crunch.adapters.BudgetPagerAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.adapters.BudgetDropdownAdapter;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;
import com.coetre.crunch.utils.Logger;

import java.util.List;

public class OverviewFragment extends Fragment implements ActionBar.OnNavigationListener, FragmentInterface {
    private BudgetPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private BudgetPagerAdapter adapter;
    private Runnable adapterRunnable;
    private boolean created = false;
    private final Handler handler = new Handler();
    private boolean shouldShowMenu = true;
    private List<Budget> budgets;
    private SharedPreferences prefs;
    private View emptyView;

    public OverviewFragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_overview, container, false);

        prefs = getActivity().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);

        // Set up the action bar.
        final ActionBar actionBar = getActivity().getActionBar();
        if (actionBar != null) {
            budgets = new Select().from(Budget.class).where("Deleted != 1").execute();

            // Create the adapter that will return a fragment for each of the three
            // primary sections of the app.
            mSectionsPagerAdapter = new BudgetPagerAdapter(getActivity().getSupportFragmentManager(), budgets);

            emptyView = rootView.findViewById(android.R.id.empty);

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
            setAdapter(mSectionsPagerAdapter);

            if (budgets.size() > 0) {
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            }

            // When swiping between different sections, select the corresponding
            // tab. We can also use ActionBar.Tab#select() to do this if we have
            // a reference to the Tab.
            mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    actionBar.setSelectedNavigationItem(position);
                }
            });

            setNavOptions();
            actionBar.setDisplayShowTitleEnabled(false);

            setHasOptionsMenu(true);
        }

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(adapterRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (adapterRunnable != null) handler.post(adapterRunnable);
        created = true;
    }

    /**
     * Set the ViewPager adapter from this or from a subclass.
     *
     * @param pagerAdapter This is a FragmentStatePagerAdapter due to the way it creates the TAG for the
     *                     Fragment.
     * @author chris.jenkins
     */
    protected void setAdapter(BudgetPagerAdapter pagerAdapter) {
        this.adapter = pagerAdapter;
        adapterRunnable = new Runnable() {
            @Override
            public void run() {
                mViewPager.setAdapter(adapter);
            }
        };
        if (created) {
            handler.post(adapterRunnable);
        }
    }

    @Override
    public void hideMenuActions() {
        shouldShowMenu = false;
        final ActionBar actionBar = getActivity().getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }

    @Override
    public void showMenuActions() {
        shouldShowMenu = true;
        final ActionBar actionBar = getActivity().getActionBar();
        if (budgets.size() > 0) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        }
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        mViewPager.setCurrentItem(i);
        if (created) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(Config.PREFS_OVERVIEW_BUDGET_INDEX, budgets.get(i).getId());
            editor.commit();
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (budgets.size() > 0) {
            inflater.inflate(R.menu.overview, menu);
        } else {
            inflater.inflate(R.menu.overview_empty, menu);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_add_budget);
        if (item != null) {
            item.setVisible(shouldShowMenu);
        }
        item = menu.findItem(R.id.action_add_transaction);
        if (item != null) {
            item.setVisible(shouldShowMenu);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        long id = 0;
        if (budgets.size() > 0) {
            id = budgets.get(mViewPager.getCurrentItem()).getId();
        }
        switch (item.getItemId()) {
            case R.id.action_add_budget:
                intent = new Intent(getActivity(), AddBudgetActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_add_transaction:
                intent = new Intent(getActivity(), AddTransactionActivity.class);
                intent.putExtra(AddTransactionActivity.DATA_BUDGET_ID, id);
                startActivity(intent);
                return true;
            case R.id.action_delete:
                deleteBudget(id);
                return true;
            case R.id.action_edit:
                intent = new Intent(getActivity(), EditBudgetActivity.class);
                intent.putExtra(EditBudgetActivity.DATA_BUDGET_ID, id);
                startActivity(intent);
                break;
            case R.id.action_view_history:
                intent = new Intent(getActivity(), ViewLogsActivity.class);
                intent.putExtra(ViewLogsActivity.DATA_BUDGET_ID, id);
                startActivity(intent);
                break;
            case R.id.action_reset:
                resetBudget(id);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Updates the UI when data changes
     */
    public void update() {
        int currentLength = budgets.size();
        budgets = new Select().from(Budget.class).where("Deleted != 1").execute();

        if (getActivity() != null) {
            final ActionBar actionBar = getActivity().getActionBar();
            if (actionBar != null) {
                setNavOptions();
                if ((budgets.size() == 0 || currentLength == 0) && currentLength != budgets.size()) {
                    getActivity().invalidateOptionsMenu();
                }

                if (budgets.size() > 0) {
                    emptyView.setVisibility(View.GONE);
                    mViewPager.setVisibility(View.VISIBLE);
                } else {
                    emptyView.setVisibility(View.VISIBLE);
                    mViewPager.setVisibility(View.GONE);
                }
            }
        }
        mSectionsPagerAdapter = new BudgetPagerAdapter(getActivity().getSupportFragmentManager(), budgets);
        setAdapter(mSectionsPagerAdapter);
    }

    /**
     * Deletes the budget
     *
     * @param id
     */
    private void deleteBudget(long id) {
        BudgetActions.delete(getActivity(), id, new BudgetActions.OnCompleteAction() {
            @Override
            public void completed() {
                update();
            }
        });
    }

    /**
     * Resets the budget
     *
     * @param id
     */
    private void resetBudget(long id) {
        BudgetActions.reset(getActivity(), id, new BudgetActions.OnCompleteAction() {
            @Override
            public void completed() {
                update();
            }
        });
    }

    /**
     * Sets the Navigation options for the fragment
     */
    private void setNavOptions() {
        BudgetDropdownAdapter adapter = new BudgetDropdownAdapter(getActivity(), budgets);
        final ActionBar actionBar = getActivity().getActionBar();
        if (budgets.size() > 0) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        } else {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
        actionBar.setListNavigationCallbacks(adapter, this);

        long id = prefs.getLong(Config.PREFS_OVERVIEW_BUDGET_INDEX, 0);
        int index = -1;
        for (Budget budget : budgets) {
            ++index;
            if (budget.getId().equals(id)) {
                break;
            }
        }
        if (index >= 0) {
            mViewPager.setCurrentItem(index);
            actionBar.setSelectedNavigationItem(index);
        }
    }
}
