package com.coetre.crunch.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.coetre.crunch.EditTransactionActivity;
import com.coetre.crunch.R;
import com.coetre.crunch.adapters.TransactionAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;
import com.coetre.crunch.utils.Formatter;
import com.coetre.crunch.utils.Logger;

import java.util.ArrayList;
import java.util.List;

public class DetailFragment extends Fragment implements EntityChangeManager.EntityChangeListener,AdapterView.OnItemClickListener {
    public static final String DATA_BUDGET_ID = "budgetId";

    private GridView transactionList;
    private TextView currentView;
    private TextView totalView;
    private TextView percentageView;
    private View emptyView;
    private LinearLayout amountContainer;
    private Budget budget;

    public DetailFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        long id = getArguments().getLong(DATA_BUDGET_ID, 0);
        if(id > 0) {
            budget = Model.load(Budget.class, id);
            update();
        }
        EntityChangeManager.add(Transaction.class, this);
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EntityChangeManager.remove(Transaction.class, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        percentageView = ((TextView)rootView.findViewById(R.id.percentage));
        currentView = ((TextView)rootView.findViewById(R.id.current));
        totalView = ((TextView)rootView.findViewById(R.id.total));
        emptyView = rootView.findViewById(android.R.id.empty);
        transactionList = ((GridView)rootView.findViewById(R.id.transaction_list));
        amountContainer = ((LinearLayout)rootView.findViewById(R.id.amount_container));
        ((TextView)rootView.findViewById(R.id.separator)).setTypeface(Config.light);
        ((TextView)rootView.findViewById(R.id.total)).setTypeface(Config.light);

        transactionList.setOnItemClickListener(this);
        transactionList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        transactionList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            private List<Transaction> selectedTransactions;

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                if(checked) {
                    selectedTransactions.add(((TransactionAdapter)transactionList.getAdapter()).getItem(position));
                } else {
                    selectedTransactions.remove(((TransactionAdapter)transactionList.getAdapter()).getItem(position));
                }
                mode.setTitle(Integer.toString(selectedTransactions.size())+" selected");
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        deleteSelectedItems(selectedTransactions);
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate the menu for the CAB
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.transaction_context, menu);
                selectedTransactions = new ArrayList<Transaction>();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        update();
        super.onResume();
    }

    /**
     * Populate the view
     */
    private void update() {
        if(amountContainer != null && this.budget != null) {
            amountContainer.setVisibility(View.VISIBLE);
            transactionList.setVisibility(View.VISIBLE);

            List<Transaction> transactions = new Select().from(Transaction.class).where("Budget = ?",budget.getId()).orderBy("id DESC").execute();
            TransactionAdapter adapter = new TransactionAdapter(getView().getContext(), R.layout.view_card, transactions);
            transactionList.setAdapter(adapter);

            emptyView.setVisibility(transactions.size() > 0 ? View.GONE : View.VISIBLE);

            totalView.setText("$"+Integer.toString(budget.getAmount()));

            int available = budget.getAmount()-budget.getCurrentAmount();
            if(available < 0) {
                available = 0;
            }
            currentView.setText("$"+available);

            Formatter.setPercentageView(getActivity(), budget, percentageView);
        }
    }

    /**
     * Delete selected transactions
     * @param selectedTransactions
     */
    private void deleteSelectedItems(List<Transaction> selectedTransactions) {
        if(selectedTransactions != null && selectedTransactions.size() > 0) {
            int total = 0;
            for(Transaction transaction : selectedTransactions) {
                 total += Math.round(transaction.getAmount());
                Model.delete(Transaction.class, transaction.getId());
            }
            budget.setCurrentAmount(budget.getCurrentAmount()-total);
            budget.save();
            update();
        }
    }

    @Override
    public void change(Class<?> cls) {
        budget =  Budget.load(Budget.class, budget.getId());
        update();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getActivity(), EditTransactionActivity.class);
        intent.putExtra(EditTransactionActivity.DATA_TRANSACTION_ID, ((TransactionAdapter)transactionList.getAdapter()).getItem(i).getId());
        startActivity(intent);
    }
}
