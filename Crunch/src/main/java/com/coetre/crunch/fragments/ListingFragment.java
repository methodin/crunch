package com.coetre.crunch.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.coetre.crunch.AddBudgetActivity;
import com.coetre.crunch.AddTransactionActivity;
import com.coetre.crunch.MainActivity;
import com.coetre.crunch.R;
import com.coetre.crunch.ViewBudgetActivity;
import com.coetre.crunch.actions.BudgetActions;
import com.coetre.crunch.adapters.BudgetAdapter;
import com.coetre.crunch.adapters.BudgetPagerAdapter;
import com.coetre.crunch.adapters.TransactionAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;

import java.util.ArrayList;
import java.util.List;

public class ListingFragment extends Fragment implements FragmentInterface,AdapterView.OnItemClickListener {
    private GridView budgetList;
    private List<Budget> selectedBudgets;
    private boolean shouldShowMenu = true;
    private View emptyView;

	public ListingFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_listing, container, false);

        // Set up the action bar.
        final ActionBar actionBar = getActivity().getActionBar();
        if(actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            setHasOptionsMenu(true);
        }

        emptyView = rootView.findViewById(android.R.id.empty);

        budgetList = ((GridView)rootView.findViewById(R.id.budget_list));
        budgetList.setOnItemClickListener(this);
        budgetList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        budgetList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                if(checked) {
                    selectedBudgets.add(((BudgetAdapter)budgetList.getAdapter()).getItem(position));
                } else {
                    selectedBudgets.remove(((BudgetAdapter)budgetList.getAdapter()).getItem(position));
                }
                mode.setTitle(Integer.toString(selectedBudgets.size())+" selected");
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        deleteSelectedItems();
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate the menu for the CAB
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.budget_context, menu);
                selectedBudgets = new ArrayList<Budget>();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });

		return rootView;
	}

    @Override
    public void onResume() {
        update();
        super.onResume();
    }

    /**
     * Delete selected items
     */
    private void deleteSelectedItems() {
        if(selectedBudgets != null && selectedBudgets.size() > 0) {
            BudgetActions.delete(getActivity(), selectedBudgets, new BudgetActions.OnCompleteAction(){
                @Override
                public void completed() {
                    update();
                }
            });
        }
    }

    /**
     * Populate the view
     */
    private void update() {
        List<Budget> budgets = new Select().from(Budget.class).where("Deleted != 1").orderBy("id DESC").execute();
        BudgetAdapter adapter = new BudgetAdapter(getView().getContext(), budgets);
        budgetList.setAdapter(adapter);

        if (budgets.size() > 0) {
            emptyView.setVisibility(View.GONE);
            budgetList.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            budgetList.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideMenuActions() {
        shouldShowMenu = false;
    }

    @Override
    public void showMenuActions() {
        shouldShowMenu = true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getActivity(), ViewBudgetActivity.class);
        intent.putExtra(ViewBudgetActivity.DATA_BUDGET_ID, ((BudgetAdapter) budgetList.getAdapter()).getItem(i).getId());
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.listing, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_add_budget);
        if(item != null) {
            item.setVisible(shouldShowMenu);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch(item.getItemId()) {
            case R.id.action_add_budget:
                intent = new Intent(getActivity(), AddBudgetActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
