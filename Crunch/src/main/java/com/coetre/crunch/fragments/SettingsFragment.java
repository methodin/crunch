package com.coetre.crunch.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import com.coetre.crunch.R;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private final static String KEY_BACKUP = "backup";
    private final static String KEY_RESTORE = "restore";
    private final static String KEY_DELETE = "delete";
    private final static String KEY_HELP = "help";

    public static final String KEY_PREF_TIMER = "pref_timer";

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_PREF_TIMER)) {
            Intent intent = new Intent();
            intent.setAction("com.coetre.rift.intent.action.START_TIMER");
            SettingsFragment.this.getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Preference button;

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        /*
        EasyTracker.getInstance().setContext(getActivity().getApplicationContext());
        EasyTracker.getTracker().sendView("/SettingsFragment");*/

        button = findPreference(KEY_HELP);
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }

        button = findPreference("version");
        if(button != null) {
            String versionName = "Unknown";
            try {
                versionName = getActivity().getPackageManager()
                        .getPackageInfo(getActivity().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
            }

            button.setSummary(versionName);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if(preference.getKey().equals(KEY_HELP)) {
            String url = "http://www.coetre.com/products/crunch/support";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

        return true;
    }
}