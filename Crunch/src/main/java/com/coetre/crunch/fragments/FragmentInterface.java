package com.coetre.crunch.fragments;

public interface FragmentInterface {
    public void hideMenuActions();
    public void showMenuActions();
}