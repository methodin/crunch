package com.coetre.crunch;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.coetre.crunch.actions.BudgetActions;
import com.coetre.crunch.adapters.TransactionAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Alert;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.EntityChangeManager;
import com.coetre.crunch.utils.Formatter;

import java.util.ArrayList;
import java.util.List;

public class ViewBudgetActivity extends Activity implements EntityChangeManager.EntityChangeListener, AdapterView.OnItemClickListener {
    public static final String DATA_BUDGET_ID = "budgetId";
    private Budget budget;
    private GridView transactionList;
    private View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        emptyView = findViewById(android.R.id.empty);

        long budgetId = getIntent().getLongExtra(DATA_BUDGET_ID, 0);
        if (budgetId > 0) {
            budget = Model.load(Budget.class, budgetId);

            transactionList = ((GridView) findViewById(R.id.transaction_list));
            transactionList.setOnItemClickListener(this);
            transactionList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            transactionList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                private List<Transaction> selectedTransactions;

                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                      long id, boolean checked) {
                    if (checked) {
                        selectedTransactions.add(((TransactionAdapter) transactionList.getAdapter()).getItem(position));
                    } else {
                        selectedTransactions.remove(((TransactionAdapter) transactionList.getAdapter()).getItem(position));
                    }
                    mode.setTitle(Integer.toString(selectedTransactions.size()) + " selected");
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    // Respond to clicks on the actions in the CAB
                    switch (item.getItemId()) {
                        case R.id.action_delete:
                            deleteSelectedItems(selectedTransactions);
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate the menu for the CAB
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.transaction_context, menu);
                    selectedTransactions = new ArrayList<Transaction>();
                    return true;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }
            });

            update();
            EntityChangeManager.add(Budget.class, this);
            EntityChangeManager.add(Transaction.class, this);
        } else {
            finish();
        }
    }

    /**
     * Delete selected transactions
     *
     * @param selectedTransactions
     */
    private void deleteSelectedItems(List<Transaction> selectedTransactions) {
        if (selectedTransactions != null && selectedTransactions.size() > 0) {
            int total = 0;
            for (Transaction transaction : selectedTransactions) {
                total += transaction.getAmount();
                Model.delete(Transaction.class, transaction.getId());
            }
            budget.setCurrentAmount(budget.getCurrentAmount() - total);
            budget.save();
            update();
        }
    }

    /**
     * Updates the UI when data has changed
     */
    private void update() {
        setTitle(budget.getName());

        TextView textView = (TextView) findViewById(R.id.current);
        int available = budget.getAmount() - budget.getCurrentAmount();
        if (available < 0) {
            available = 0;
        }
        textView.setText("$" + available);

        textView = (TextView) findViewById(R.id.total);
        textView.setText("$" + budget.getAmount());
        textView.setTypeface(Config.light);

        textView = (TextView) findViewById(R.id.separator);
        textView.setTypeface(Config.light);

        textView = (TextView) findViewById(R.id.percentage);
        Formatter.setPercentageView(getBaseContext(), budget, textView);

        List<Transaction> transactions = new Select().from(Transaction.class).where("Budget = ?", budget.getId()).orderBy("id DESC").execute();
        TransactionAdapter adapter = new TransactionAdapter(getBaseContext(), R.layout.view_card, transactions);
        transactionList.setAdapter(adapter);

        emptyView.setVisibility(transactions.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id = getIntent().getLongExtra(DATA_BUDGET_ID, 0);
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_edit:
                intent = new Intent(this, EditBudgetActivity.class);
                intent.putExtra(EditBudgetActivity.DATA_BUDGET_ID, id);
                startActivity(intent);
                break;
            case R.id.action_add_transaction:
                intent = new Intent(this, AddTransactionActivity.class);
                intent.putExtra(AddTransactionActivity.DATA_BUDGET_ID, id);
                startActivity(intent);
                return true;
            case R.id.action_delete:
                deleteBudget(id);
                return true;
            case R.id.action_reset:
                resetBudget(id);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Deletes the current budget and finishes the activity
     *
     * @param id
     */
    private void deleteBudget(long id) {
        BudgetActions.delete(this, id, new BudgetActions.OnCompleteAction() {
            @Override
            public void completed() {
                finish();
            }
        });
    }

    /**
     * Resets the current budget
     *
     * @param id
     */
    private void resetBudget(long id) {
        BudgetActions.reset(this, id, new BudgetActions.OnCompleteAction() {
            @Override
            public void completed() {
                update();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_budget, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        EntityChangeManager.remove(Budget.class, this);
        EntityChangeManager.remove(Transaction.class, this);
        super.onDestroy();
    }

    @Override
    public void change(Class<?> cls) {
        budget = Model.load(Budget.class, budget.getId());
        update();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, EditTransactionActivity.class);
        intent.putExtra(EditTransactionActivity.DATA_TRANSACTION_ID, ((TransactionAdapter) transactionList.getAdapter()).getItem(i).getId());
        startActivity(intent);
    }
}