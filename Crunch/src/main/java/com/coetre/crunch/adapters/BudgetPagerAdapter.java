package com.coetre.crunch.adapters;

import android.os.Bundle;
import android.support.v4.app.*;

import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.fragments.DetailFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BudgetPagerAdapter extends FragmentStatePagerAdapter {
    private List<Budget> budgets;
    private ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    final private FragmentManager fragmentManager;

    public BudgetPagerAdapter(FragmentManager fm, List<Budget> budgets) {
        super(fm);
        this.fragmentManager = fm;
        this.budgets = budgets;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        Fragment fragment = new DetailFragment();
        bundle.putLong(DetailFragment.DATA_BUDGET_ID, this.budgets.get(position).getId());
        fragment.setArguments(bundle);
        fragments.add(fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return this.budgets.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return budgets.get(position).getName();
    }

    /**
     * Clear the adapter
     */
    public void clear() {
        final FragmentTransaction trans = fragmentManager.beginTransaction();
        for (int i = 0; i < fragments.size(); i++) {
            trans.remove(fragments.get(i));
        }
        trans.commit();
    }
}