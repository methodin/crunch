package com.coetre.crunch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.coetre.crunch.entities.DrawerItem;

import java.util.List;

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {
    private final int resource;

    public DrawerAdapter(Context context, int resource, List<DrawerItem> items) {
        super(context,resource,items);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(this.resource, parent, false);
        }

        ((TextView)convertView.findViewById(android.R.id.text1)).setText(this.getItem(position).getTitle());
        TextView textView = (TextView)convertView.findViewById(android.R.id.text2);
        if(this.getItem(position).hasIndicator()) {
            textView.setText(Integer.toString(this.getItem(position).getCount()));
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

        return convertView;
    }
}