package com.coetre.crunch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.coetre.crunch.R;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.BudgetArchive;
import com.coetre.crunch.entities.TransactionArchive;
import com.coetre.crunch.utils.Config;
import com.coetre.crunch.utils.Formatter;

import java.text.DecimalFormat;
import java.util.List;

public class BudgetArchiveAdapter extends ArrayAdapter<BudgetArchive> {
    private int resourceId;
    private final DecimalFormat formatter = new DecimalFormat("#,###");

    public BudgetArchiveAdapter(Context context, List<BudgetArchive> objects) {
        super(context, R.layout.budget_card, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BudgetArchive item = this.getItem(position);

        final TextView percentageView;
        final TextView descriptionView;
        final TextView amountView;

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.budget_card, parent, false);
            amountView = (TextView) convertView.findViewById(R.id.amount);
            descriptionView = (TextView) convertView.findViewById(R.id.description);
            percentageView = (TextView) convertView.findViewById(R.id.percentage);
            percentageView.setTypeface(Config.light);
            convertView.setTag(new ViewHolder(amountView, descriptionView, percentageView));
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            amountView = holder.getAmountView();
            descriptionView = holder.getDescriptionView();
            percentageView = holder.getPercentageView();
        }

        if(amountView != null) {
            amountView.setText("$" + item.getCurrentAmount());
        }

        if(descriptionView != null) {
            descriptionView.setText(item.getBudget().getName());
            descriptionView.setTypeface(Config.light);
        }

        Formatter.setPercentageView(getContext(), item, percentageView, R.string.percent_of_budget_archive);

        return convertView;
    }

    private static class ViewHolder {
        private final TextView amountView;
        private final TextView descriptionView;
        private final TextView percentageView;

        public ViewHolder(TextView amountView, TextView descriptionView, TextView percentageView) {
            this.amountView = amountView;
            this.descriptionView = descriptionView;
            this.percentageView = percentageView;
        }

        private TextView getAmountView() {
            return amountView;
        }

        private TextView getDescriptionView() {
            return descriptionView;
        }

        private TextView getPercentageView() {
            return percentageView;
        }
    }
}