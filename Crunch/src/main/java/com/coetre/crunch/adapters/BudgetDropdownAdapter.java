package com.coetre.crunch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.coetre.crunch.R;
import com.coetre.crunch.entities.Budget;

import java.util.List;

public class BudgetDropdownAdapter extends ArrayAdapter<Budget> {
    private Context mContext;

    public BudgetDropdownAdapter(Context context, List<Budget> items) {
        super(context, R.layout.nav_list_item, items);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.nav_list_item, null);
        }
        ((TextView) convertView.findViewById(android.R.id.text1))
                .setText(this.getItem(position).getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    android.R.layout.simple_list_item_1, null);
        }
        ((TextView) convertView.findViewById(android.R.id.text1))
                .setText(this.getItem(position).getName());
        return convertView;
    }

}