package com.coetre.crunch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.coetre.crunch.R;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.entities.TransactionInterface;
import com.coetre.crunch.utils.Config;

import java.text.DecimalFormat;
import java.util.List;

public class TransactionAdapter extends ArrayAdapter<Transaction> {
    private int resourceId;
    private final DecimalFormat formatter = new DecimalFormat("#,###");
	
	public TransactionAdapter(Context context, int textViewResourceId, List<Transaction> objects) {
		super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        Transaction item = this.getItem(position);
		
		if(convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
                    resourceId, parent, false);
		}

        final TextView amountView = ((TextView) convertView.findViewById(R.id.amount));
        if(amountView != null) {
            amountView.setText("$"+formatter.format(item.getAmount()));
        }

        final TextView descriptionView = ((TextView) convertView.findViewById(R.id.description));
        if(descriptionView != null) {
            descriptionView.setText(item.getDescription());
            descriptionView.setTypeface(Config.light);
        }
		
		return convertView;
	}
}