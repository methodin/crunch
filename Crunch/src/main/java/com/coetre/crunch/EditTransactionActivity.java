package com.coetre.crunch;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.activeandroid.Model;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Alert;
import com.coetre.crunch.utils.EntityChangeManager;

public class EditTransactionActivity extends Activity implements View.OnClickListener {
    public final static String DATA_TRANSACTION_ID = "transactionId";
    private Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        Button addButton = (Button) findViewById(R.id.add_button);
        addButton.setText(R.string.button_save);
        addButton.setOnClickListener(this);

        findViewById(R.id.cancel_button).setOnClickListener(this);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TRANSACTION_ID)) {
            long id = getIntent().getLongExtra(DATA_TRANSACTION_ID, 0);
            transaction = Model.load(Transaction.class, id);

            EditText editText = (EditText) findViewById(R.id.transaction_amount);
            editText.setText(Double.toString(transaction.getAmount()));

            editText = (EditText) findViewById(R.id.transaction_name);
            editText.setText(transaction.getDescription());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.add_button:
                String name = ((EditText) findViewById(R.id.transaction_name)).getText().toString();
                String amount = ((EditText) findViewById(R.id.transaction_amount)).getText().toString();

                double actualAmount = 0;
                try {
                    actualAmount = Double.parseDouble(amount);
                } catch (NumberFormatException e) {
                }

                // Form validation
                if (name.equals("")) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_transaction_name));
                } else if(actualAmount <= 0) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_transaction_amount));
                } else {
                    int diff = (int)(Math.round(actualAmount) - Math.round(transaction.getAmount()));
                    transaction.setAmount(actualAmount);
                    transaction.setDescription(name);
                    transaction.save();

                    if(diff != 0) {
                        Budget budget = Budget.load(Budget.class, transaction.getBudget().getId());
                        budget.setCurrentAmount(budget.getCurrentAmount()+diff);
                        budget.save();
                    }

                    EntityChangeManager.notify(Transaction.class);
                    EntityChangeManager.notify(Budget.class);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
    }
}