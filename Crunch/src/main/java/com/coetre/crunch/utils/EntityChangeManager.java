package com.coetre.crunch.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Manager instance to handle Task objects
 */
public class EntityChangeManager {
    /**
     * List of Tasks
     */
    private static final HashMap<String, ArrayList<EntityChangeListener>> classes = new HashMap<String, ArrayList<EntityChangeListener>>();

    /**
     * Add a Task to the list
     *
     * @param cls
     * @param listener
     */
    public static void add(Class<?> cls, EntityChangeListener listener) {
        String key = cls.getSimpleName();
        if (!classes.containsKey(key)) {
            classes.put(key, new ArrayList<EntityChangeListener>());
        }
        classes.get(key).add(listener);
    }

    /**
     * Destroy the change listener
     *
     * @param cls
     * @param listener
     */
    public static void remove(Class<?> cls, EntityChangeListener listener) {
        String key = cls.getSimpleName();
        if (classes.containsKey(key)) {
            classes.get(key).remove(listener);
        }
    }

    /**
     * Notify all listeners that an item changed
     *
     * @param cls
     */
    public static void notify(Class<?> cls) {
        String key = cls.getSimpleName();
        if (classes.containsKey(key)) {
            ArrayList<EntityChangeListener> listeners = classes.get(key);
            for(int i=0;i<listeners.size();i++) {
                if(listeners.get(i) != null) {
                    listeners.get(i).change(cls);
                }
            }
        }
    }

    public interface EntityChangeListener {
        /**
         * Entity class has changed
         */
        public void change(Class<?> cls);
    }
}