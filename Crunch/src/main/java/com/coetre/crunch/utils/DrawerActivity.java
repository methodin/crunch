package com.coetre.crunch.utils;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.activeandroid.query.Select;
import com.coetre.crunch.R;
import com.coetre.crunch.SettingsActivity;
import com.coetre.crunch.adapters.DrawerAdapter;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.DrawerItem;
import com.coetre.crunch.fragments.FragmentInterface;
import com.coetre.crunch.fragments.ListingFragment;
import com.coetre.crunch.fragments.OverviewFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class DrawerActivity extends FragmentActivity {
    private SharedPreferences prefs;
    private ActionBarDrawerToggle drawerToggle;
    protected DrawerLayout drawerLayout;
    protected String[] drawerItems;
    protected ListView drawerList;
    private CharSequence title = "";
    protected Fragment fragment = null;
    private boolean isDrawerLocked = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);

        prefs = getBaseContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);

        Config.mode = prefs.getInt(Config.PREFS_KEY_MODE, Config.MODE_DETAIL);

        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        if(((ViewGroup.MarginLayoutParams)frameLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawer_size)) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, drawerList);
            drawerLayout.setScrimColor(Color.TRANSPARENT);
            isDrawerLocked = true;
        }

        // Set the adapter for the list view
        drawerItems = getResources().getStringArray(R.array.drawer_options);

        final ActionBar actionBar = getActionBar();

        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_closed  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                if(actionBar != null) {
                    actionBar.setTitle(title);
                }
                ((FragmentInterface)fragment).showMenuActions();
                invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                if(actionBar != null) {
                    actionBar.setTitle("Select Option");
                }
                ((FragmentInterface)fragment).hideMenuActions();
                invalidateOptionsMenu();
            }
        };

        ArrayList<DrawerItem> drawerListValues = new ArrayList<DrawerItem>();
        for(String drawerItem : drawerItems) {
            if(drawerItem.equals(getResources().getString(R.string.drawer_option_settings))) {
                drawerListValues.add(new DrawerItem(drawerItem, 0, false));
            } else {
                drawerListValues.add(new DrawerItem(drawerItem, 0, true));
            }
        }
        drawerList.setAdapter(new DrawerAdapter(this,
                R.layout.drawer_list_item, drawerListValues));

        if(!isDrawerLocked) {
            // Set the drawer toggle as the DrawerListener
            drawerLayout.setDrawerListener(drawerToggle);

            if(actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isDrawerLocked() {
        return isDrawerLocked;
    }
}