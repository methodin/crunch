package com.coetre.crunch.utils;

import android.content.Context;
import android.widget.TextView;

import com.coetre.crunch.R;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.BudgetInterface;

public class Formatter {
    /**
     * Sets the TextView to the percentage text associated with the budget string configuration and
     * formats the color
     * @param context
     * @param budget
     * @param textView
     */
    public static void setPercentageView(Context context, BudgetInterface budget, TextView textView) {
        setPercentageView(context,budget,textView,R.string.percent_of_budget);
    }
    /**
     * Sets the TextView to the percentage text associated with the budget string configuration and
     * formats the color
     * @param context
     * @param budget
     * @param textView
     * @param resourceId
     */
    public static void setPercentageView(Context context, BudgetInterface budget, TextView textView, int resourceId) {
        int available = 100-(int)(((double)budget.getCurrentAmount()/(double)budget.getAmount())*100);
        textView.setText(Integer.toString(available)+context.getResources().getString(resourceId));
        if(available <= 10) {
            textView.setTextColor(context.getResources().getColor(R.color.percentage_over));
        } else if(available <= 20) {
            textView.setTextColor(context.getResources().getColor(R.color.percentage_close));
        } else {
            textView.setTextColor(context.getResources().getColor(R.color.percentage_good));
        }
    }
}