package com.coetre.crunch.utils;

import android.content.Context;
import android.widget.Toast;

public class Alert {
    /**
     * Shows an alert message
     * @param context
     * @param text
     */
    public static void show(Context context, String text) {
        (Toast.makeText(context, text, Toast.LENGTH_SHORT))
                .show();
    }
}