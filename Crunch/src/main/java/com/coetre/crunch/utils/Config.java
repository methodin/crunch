package com.coetre.crunch.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import com.coetre.crunch.R;

public class Config {
    /**
     * The shared prefs editor name
     */
    public final static String PREFS_NAME = "com.coetre.crunch";
    public final static String PREFS_OVERVIEW_BUDGET_INDEX= "overviewBudgetIndex";
    public final static String PREFS_BUDGET_ID = "budgetId";
    public final static String PREFS_KEY_MODE = "mode";

    /**
     * Drawer task mode
     */
    public final static int MODE_DETAIL = 0;
    public final static int MODE_LISTING = 1;
    public final static int MODE_ARCHIVE = 2;
    public final static int MODE_SETTINGS = 3;

    public static int mode = Config.MODE_DETAIL;
        
    public static Typeface light = Typeface.create("sans-serif-light", Typeface.NORMAL);
}