package com.coetre.crunch.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "TransactionArchive")
public class TransactionArchive extends Model implements TransactionInterface {
    /**
     * The description of the transaction
     */
    @Column(name = "Description")
    private String description = "";

    /**
     * The amount of the transaction
     */
    @Column(name = "Amount")
    private double amount;

    /**
     * The parent budget
     */
    @Column(name = "BudgetArchive")
    private BudgetArchive budgetArchive;

    public TransactionArchive(Transaction transaction, BudgetArchive budgetArchive) {
        this.description = transaction.getDescription();
        this.amount = transaction.getAmount();
        this.budgetArchive = budgetArchive;
    }

    public TransactionArchive() {
        super();
    }

    public BudgetArchive getBudgetArchive() {
        return budgetArchive;
    }

    public double getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }
}