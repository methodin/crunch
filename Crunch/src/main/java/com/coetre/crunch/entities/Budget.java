package com.coetre.crunch.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Budget")
public class Budget extends Model implements BudgetInterface {
    /**
     * The name of the budget
     */
    @Column(name = "Name")
    private String name = "";

    /**
     * The target amount of the budget
     */
    @Column(name = "Amount")
    private int amount;

    /**
     * The current amount of the budget
     */
    @Column(name = "CurrentAmount")
    private int currentAmount;

    /**
     * Is the budget active?
     */
    @Column(name = "Deleted")
    private boolean deleted;

    public Budget(String name, int amount) {
        this.name = name;
        this.amount = amount;
        this.deleted = false;
        this.currentAmount = 0;
    }

    public Budget() {
        super();
    }

    public int getAmount() {
        return amount;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean closed) {
        this.deleted = closed;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(int currentAmount) {
        this.currentAmount = currentAmount;
    }

    public List<Transaction> getTransactions() {
        return getMany(Transaction.class, "Budget");
    }
}