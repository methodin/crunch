package com.coetre.crunch.entities;

public interface TransactionInterface {
    public double getAmount();
    public String getDescription();
}