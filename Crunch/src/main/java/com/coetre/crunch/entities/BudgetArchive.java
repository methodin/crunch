package com.coetre.crunch.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

@Table(name = "BudgetArchive")
public class BudgetArchive extends Model implements BudgetInterface {
    /**
     * The name of the budget
     */
    @Column(name = "Name")
    private String name = "";

    /**
     * The target amount of the budget
     */
    @Column(name = "Amount")
    private int amount;

    /**
     * The current amount of the budget
     */
    @Column(name = "CurrentAmount")
    private int currentAmount;

    /**
     * The Budget reference
     */
    @Column(name = "Budget")
    private Budget budget;

    /**
     * The date of archival
     */
    @Column(name = "Date")
    private Date date;

    public BudgetArchive(Budget budget, Date date) {
        this.budget = budget;
        this.name = budget.getName();
        this.amount = budget.getAmount();
        this.currentAmount = budget.getCurrentAmount();
        this.date = date;
    }

    public BudgetArchive() {
        super();
    }

    public Budget getBudget() {
        return budget;
    }

    public int getAmount() {
        return amount;
    }

    public int getCurrentAmount() {
        return currentAmount;
    }

    public Date getDate() {
        return date;
    }

    public List<TransactionArchive> getTransactions() {
        return getMany(TransactionArchive.class, "BudgetArchive");
    }
}