package com.coetre.crunch.entities;

public interface BudgetInterface {
    public int getCurrentAmount();
    public int getAmount();
}