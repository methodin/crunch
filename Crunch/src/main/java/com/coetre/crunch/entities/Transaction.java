package com.coetre.crunch.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "TransactionList")
public class Transaction extends Model implements TransactionInterface {
    /**
     * The description of the transaction
     */
    @Column(name = "Description")
	private String description = "";

    /**
     * The amount of the transaction
     */
    @Column(name = "Amount")
	private double amount;

    /**
     * The parent budget
     */
    @Column(name = "Budget")
    private Budget budget;
	
	public Transaction(double amount, String description, Budget budget) {
		this.description = description;
		this.amount = amount;
        this.budget = budget;
	}
	
	public Transaction() {
		super();
	}

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}