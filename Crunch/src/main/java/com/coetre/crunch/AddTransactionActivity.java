package com.coetre.crunch;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.activeandroid.Model;
import com.coetre.crunch.entities.Budget;
import com.coetre.crunch.entities.Transaction;
import com.coetre.crunch.utils.Alert;
import com.coetre.crunch.utils.EntityChangeManager;

public class AddTransactionActivity extends Activity implements View.OnClickListener {
    public final static String DATA_BUDGET_ID = "budgetId";
    private Budget budget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        findViewById(R.id.cancel_button).setOnClickListener(this);
        findViewById(R.id.add_button).setOnClickListener(this);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_BUDGET_ID)) {
            long id = getIntent().getLongExtra(DATA_BUDGET_ID, 0);
            budget = Model.load(Budget.class, id);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.add_button:
                String name = ((EditText) findViewById(R.id.transaction_name)).getText().toString();
                String amount = ((EditText) findViewById(R.id.transaction_amount)).getText().toString();

                double actualAmount = 0;
                try {
                    actualAmount = Double.parseDouble(amount);
                } catch (NumberFormatException e) {
                }

                // Form validation
                if (name.equals("")) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_transaction_name));
                } else if(actualAmount <= 0) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.empty_transaction_amount));
                } else {
                    new Transaction(actualAmount, name, budget).save();
                    int roundedAmount = (int) Math.round(actualAmount);
                    budget.setCurrentAmount(budget.getCurrentAmount()+roundedAmount);
                    budget.save();
                    EntityChangeManager.notify(Transaction.class);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
    }
}